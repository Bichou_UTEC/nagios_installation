#!/bin/bash

############# INITIALISATION DES VARIABLES #############
PileFace(){


stats=0

pile=0
face=0

pile1=0
face1=0

var1="face"
var2="pile"



############# MESSAGE #############

echo -e -n "\033[07mBienvenue sur le jeu de la pièce !\n\n\n\033[0m"
 
############# TIRAGE #############

tirage () {
  echo -e -n "\033[01mCombien de tirage par joueur voulez-vous choisir ? \033[0m"
  
  while read -r nbTirage
  do
    re='^[0-9]+$'
    if [[ $nbTirage =~ $re ]] ; 
      then
        echo -e "Vous avez choisi $nbTirage partie(s).\n"
        echo -e -n "\033[07mPlace au jeu !\n\n\n\033[0m"
        break
    else  
      echo "Veuillez saisir une valeur numérique : "
    fi
  done
}



############# CHOIX #############

choix() {
  echo -e -n "\033[01mPile ou face ? Quel est votre choix ? : \033[0m"
  sleep 1
  while read -r pileFace
  do
    echo -e "\033[44mVous avez choisi ${pileFace^^}.\033[0m"
    printf '\n'
    sleep 1
    if [ "${pileFace,,}" = "$var2" ] ;
    then
      pile=$(($pile+1))
      break
     
    elif [ "${pileFace,,}" = "$var1" ] ;
    then
      face=$(($face+1))
      break
     
    else
      echo Veuillez réessayer :
    fi
  done
}

############# ANIMATION #############

spinner() {
    local i sp n
    sp='/-\|'
    n=${#sp}
    printf ' '
    while sleep 0.1; do
        printf "%s\b" "${sp:i++%n:1}"
    done
}

############# COMPTE A REBOUR #############

decompte() {
  echo -e "\033[01mLa pièce est tirée ...\033[0m"
  echo " "
  spinner &
  i=$1
  while [[ $i -ge 0 ]]
  do
    echo -e "\033[01m\r "$i" \c\033[0m"
    sleep 1
    i=$(expr $i - 1 )
  done
  kill "$!"
  printf '\n'
  echo -e "\n\033[01mLa pièce va tomber ...\033[0m"
  printf '\n'
  sleep 1
 
}

############# GENRATEUR #############

generateur() {
  result=$(shuf -i 0-1 -n 1 | sed -e 's/1/Pile/' -e 's/0/Face/')
  echo -e "\033[44m${result^^}\033[0m\n"
  sleep 1
 
  if [ "${result,,}" = "$var2" ] ;
  then
    pile1=$(($pile1+1))
   
  elif [ "${result,,}" = "$var1" ] ;
  then
    face1=$(($face1+1))
   
  fi
 
  if [ "${pileFace,,}" = "${result,,}" ] ;
  then
    echo -e "\033[42mVous avez gagné !\033[0m"
    sleep 1
   
  else
    echo -e "\033[41mVous avez perdu !\033[0m"
    sleep 1
   
  fi
}

############# STATS #############

pourcentage()
{
TotalPileFace=$(($face1+$pile1))
PourcentageFace=$(echo "$face1 / $TotalPileFace * 100" | bc -l)
PourcentagePile=$(echo 100 - $PourcentageFace | bc -l)

echo -e -n "\033[07mVoici les statistiques de votre partie :\n\n\n\033[0m"
sleep 1
echo -e "Vous avez effectué \033[44m$TotalPileFace\033[0m lancés."
echo -e "La propabilité que FACE tombe était de \033[44m${PourcentageFace::5}%\033[0m."
echo -e "La propabilité que pile tombe était de \033[44m${PourcentagePile::5}%\033[0m."
}

############# BOUCLE TIRAGE #############

piece() {
  while [ "$stats" -lt "$nbTirage" ]
  do
    choix
    decompte 3
    generateur
    stats=$(($stats+1))
    echo " "
  done
}

############# APPELS FONCTIONS #############

tirage 
piece
sleep 1
pourcentage

exit
}
PileFace
